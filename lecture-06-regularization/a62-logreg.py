import numpy as np
from lr import *
from sklearn.datasets import *
from plotdata import *
from matplotlib.pyplot import *

def quad_features(X):
    return np.hstack((X, X**2))

def polynomial_features(X):
    x = np.array([X[:,0]]).T
    y = np.array([X[:,1]]).T
    
    r = x
    for i in range(1,7):
        for j in range(0,i+1):
            r = np.hstack((r,x**(i-j)*y**j))
    return r   

train_data = np.loadtxt(open("xtrain-1.csv"), delimiter=",")
train_labels = np.loadtxt(open("ytrain-1.csv"), delimiter=",")
test_data = np.loadtxt(open("xtest-1.csv"), delimiter=",")
test_labels = np.loadtxt(open("ytest-1.csv"), delimiter=",")

lambda_list = [0.0, 0.048, 0.2]
i=1
for l in lambda_list:
    print "lambda regularization = "+str(l)
    lr = LogisticRegression(0.001, map_features_function=polynomial_features, lmbd=l)
    lr.fit(train_data, train_labels)
    
    train_acc = lr.score(train_data, train_labels)
    test_acc = lr.score(test_data, test_labels)
    
    print "train acc = "+str(train_acc)
    print "test acc  = "+str(test_acc)
    
    subplot(2,3,i+3)
    title ("test, l="+str(l)+" acc=%0.3f"%test_acc)
    plot_boundary_with_data(lr,test_data, test_labels)
    subplot(2,3,i)    
    title ("train, l="+str(l)+" acc=%0.3f"%train_acc)
    plot_boundary_with_data(lr,train_data, train_labels)
    i += 1
    
show()
    



