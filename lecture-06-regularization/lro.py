import numpy as np
from scipy.optimize import minimize

class LogisticRegressionO:
    
    def __init__ (self, map_features_function=None, lmbd=0.0):
        self.map_features_function = map_features_function
        self.lmbd = lmbd

    def fit(self, X, c):
        m = X.shape[1]
        
        # map features
        if self.map_features_function!=None:
            X = self.map_features_function(X)

        # add a column of ones to x matrix
        X = np.hstack((np.ones((X.shape[0],1)),X))

        # initialize weights with zeroes
        w_init = np.zeros(X.shape[1])
        
        # define cost function
        def lrcost (theta):
            # --- INSERT YOUR CODE HERE
            # compute regularized cost as (g(X*theta)-c)**2 + lambda_vector*theta**2
            # note that lambda is available at self.lambda
            cost = ?????
            # ---------
            return cost
        
        # minimize parameters with respect to the cost function
        res = minimize(lrcost, w_init, method='BFGS')
            
        self.theta = res.x
    
    def predict(self, X):
        if self.map_features_function!=None:
            X = self.map_features_function(X)
            
        # add 1 for bias
        xone = np.hstack((np.ones((X.shape[0],1)),X))
        
        # threshold on g
        return self.g(self.theta.dot(xone.T))>0.5
    
    def score(self, X, c):
        return sum(self.predict(X)==c)*1.0/X.shape[0]
    
    def g(self, x):
        # compute g = 1 / (1-e^(-w'x))
       return 1/(1+np.exp(-x))
