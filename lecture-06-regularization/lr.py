import numpy as np

class LogisticRegression:
    
    def __init__ (self, alpha, map_features_function=None, lmbd=0.0):
        self.alpha = alpha
        self.map_features_function = map_features_function
        self.lmbd = lmbd

    def fit(self, X, c):
        
        if self.map_features_function!=None:
            X = self.map_features_function(X)

        # add a column of ones to x matrix
        X = np.hstack((np.ones((X.shape[0],1)),X))

        m = X.shape[0]
        
        # initialize weights with zeroes 
        w = np.zeros(X.shape[1])
        # max number of iterations
        max_iterations=2000
        i = 0; delta = np.array([100])
        while i<max_iterations and delta.sum()**2>0.0001:

            # ---- INSERT YOUR CODE HERE -----
            # update rule w = w + ( alpha*X'*(c-g(X*w)) - lambda w )
            delta =
            w = w - delta
            # --------------------------------

            i = i + 1
            
        self.theta = w
    
    def predict(self, X):
        if self.map_features_function!=None:
            X = self.map_features_function(X)
            
        # add 1 for bias
        xone = np.hstack((np.ones((X.shape[0],1)),X))
        
        # threshold on g
        return self.g(self.theta.dot(xone.T))>0.5
    
    def score(self, X, c):
        return sum(self.predict(X)==c)*1.0/X.shape[0]
    
    def g(self, x):
        # compute g = 1 / (1-e^(-w'x))
       return 1/(1+np.exp(-x))
