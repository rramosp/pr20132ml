import numpy as np

class LogisticRegression:
    
    def __init__ (self, alpha, map_features_function=None):
        self.alpha = alpha
        self.map_features_function = map_features_function

    def fit(self, X, c):
        
        if self.map_features_function!=None:
            X = self.map_features_function(X)
        
        # add a column of ones to x matrix
        X = np.hstack((np.ones((X.shape[0],1)),X))

        m = X.shape[1]
        
        # initialize weights with zeroes
        w = np.zeros(X.shape[1])
        
        # iterations
        max_iterations=2000
        i = 0; 
 
        while i<max_iterations and delta.sum()**2>0.00001:
            # update rule w = w + alpha*X'*(c-g(X*w))
            delta = *** YOUR CODE HERE ***
            w = w + delta
            i = i + 1
            
        self.theta = w
    
    def predict(self, X):
        if self.map_features_function!=None:
            X = self.map_features_function(X)
            
        # add 1 for bias
        xone = np.hstack((np.ones((X.shape[0],1)),X))
        
        # threshold on g
        return self.g(self.theta.dot(xone.T))>0.5
    
    def score(self, X, c):
        return *** YOUR CODE HERE ***
    
    def g(self, x):
        # compute g = 1 / (1-e^(-w'x))
       return 1/(1+np.exp(-x))
