import numpy as np
from lr import *
from sklearn.datasets import *
from plotdata import *

n=200
(d,c) = make_moons(n_samples=n, noise=0.2)
lr = LogisticRegression(0.01)
lr.fit(d,c)
print "accuracy="+str(lr.score(d,c))
print "fitted weights are "+str(lr.theta)
plot_boundary_with_data(lr,d,c)
show()
    
