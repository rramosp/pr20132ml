import glob, Image
import numpy as np

imgs = None
print "loading imgs..."
files = glob.glob("face/*.jpg")
for filename in files:
  img = np.array(Image.open(filename).getdata())
  if imgs is None:
	 imgs = np.array([img])
  else:
	 imgs = np.vstack((imgs, img))

print "loaded images ",imgs.shape

