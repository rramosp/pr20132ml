clear all; close all; clc

x = load('x.dat'); 
y = load('y.dat');

m = length(y)

% *** MODIFY HERE *** Add a column of ones to x
x = ??

% Scale features and set them to zero mean
mu = mean(x);
sigma = std(x);
% *** MODIFY HERE *** scale each column by (x-mu)/sigma
x(:,2) = ???
x(:,3) = ???

% Gradient Descent 
alpha = 1;
MAX_ITR = 100;
% this will contain my final values of theta
% after I've found the best learning rate
theta_grad_descent = zeros(size(x(1,:))); 

theta = zeros(size(x(1,:)))'; % initialize fitting parameters

for num_iterations = 1:MAX_ITR
    % The gradient
    % *** MODIFY HERE *** write the formula for the gradient descent
    grad = ????
    
    % amount of change
    delta = alpha .* grad;
    
    % ** MODIFY HERE *** break loop if the norm squared of delta is less than 0.01
    ?????
        
    % Here is the actual update
    theta = theta - delta;

end

% *** MODIFY HERE *** compute the mean square error
mse = ????
num_iterations

% Estimate the price of a 1650 sq-ft, 3 br house
% *** MODIFY HERE *** use mu and sigma to normalize, use theta to estimate
price_grad_desc = ???

