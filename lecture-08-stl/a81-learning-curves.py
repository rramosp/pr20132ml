import numpy as np
from lc import *
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB

def polynomial_features(d):
	d = np.hstack((d,d**2))
	r = np.hstack((d,d**2))
	for i in range(0,d.shape[1]):
	   for j in range(i+1,d.shape[1]):
			r = np.hstack( (r, np.array([d[:,i]*d[:,j]]).T))

	return r


file_data = np.loadtxt("contaminacion-4k-9attrs.csv", delimiter=",", skiprows=1)

# extract columns 0 to 784 into a single data structure
data = file_data[:,1:10]
qdata =  polynomial_features(data)
print qdata.shape
# extract column 785 (which is column nb 784 if starting at index 0)
labels = (file_data[:,0]>6)*1

print "positive instances "+str(np.sum(labels==1))
print "negative instances "+str(np.sum(labels==0))

nb_bootstraps=10

learning_curve(KNeighborsClassifier(3), qdata, labels, nb_bootstraps=nb_bootstraps)
plt.show()
