import numpy as np
from sklearn import cross_validation

def bootstrap_score(classifier, data, targets, score_function, nb_bootstraps, test_size):
    
    scores_train = []
    scores_test  = []
    for i in np.arange(0, nb_bootstraps):

        data_train, data_test, targets_train, targets_test = cross_validation.train_test_split(data, targets, test_size=test_size, random_state=np.random.randint(1,100))
    
        classifier.fit (data_train, targets_train)
        score_train = score_function(classifier.predict(data_train), targets_train)
        score_test  = score_function(classifier.predict(data_test), targets_test)
        scores_train.append(score_train)
        scores_test.append(score_test)

    return np.vstack((scores_train, scores_test))
