import numpy as np

from matplotlib.pyplot import *

data = np.loadtxt("mnist-small.csv", delimiter=",")

# extract columns 0 to 784 into a single data structure
d=data[:,0:784]

# extract column 785 (which is column nb 784 if starting at index 0)
c=data[:,784]

# see rows 208, 56 for an ambiguous digit image

# extract one random row
i=np.int(np.random.rand(1)*d.shape[1])
print "showing row "+str(i)
xrandom = d[i]

# reshape row into 28x28 image (use nparray.reshape)
x = xrandom.reshape(28,28) 

# display image
imshow(x, aspect="equal", interpolation="nearest", cmap = cm.Greys_r)
show()




