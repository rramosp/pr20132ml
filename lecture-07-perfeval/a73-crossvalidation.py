import numpy as np
from sklearn import cross_validation
from sklearn.neighbors import KNeighborsClassifier

data = np.loadtxt("contaminacion-20k-9attrs.csv", delimiter=",", skiprows=1)

# --- YOUR CODE HERE ----
# extract columns 1 to 10 into a single data structure
train_data = ?????

# extract first column and threshold it (class 1 if >6, class 0 otherwise)
train_labels = ???
# --------------------

print "positive instances "+str(np.sum(train_labels==1))
print "negative instances "+str(np.sum(train_labels==0))

for i in [1,2,3,5,10,15]:
   c = KNeighborsClassifier(i)
   # ---- YOUR CODE HERE -----
   # use the cross_validatin.cross_val_score to perform 10-fold cross validation
   # using the "accuracy" metric
   s = cross_validation.cross_val_score(???????????????????)
   # -------------------------
   print "----- k = "+str(i)+", mean scores: "+str( s.mean())+", std: "+str(s.std())
