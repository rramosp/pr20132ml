import sys
import numpy as np
from sklearn.datasets import *
from sklearn.neighbors import KNeighborsClassifier
from plotdata import *
from sklearn.svm import *
import matplotlib.pyplot as plt

k=3
n_train = 100
n_test  = 50

(d_train,c_train) = make_moons(n_samples=n_train, noise=0.2)
(d_test,c_test) = make_moons(n_samples=n_test, noise=0.2)

def radial_kernel(X1,X2):
        # ---------------------
        #  your code here
        # use gamma = 2.0
        # ---------------------
	return r

svm = SVC(kernel="rbf", gamma=2.0)
svm.fit(d_train,c_train)
print "score train = ", svm.score(d_train, c_train)
print "score test  = ", svm.score(d_test, c_test)

plot_boundary_with_data(svm,d_train,c_train, density=100j)

# ---------------------
# your code code
# ---------------------
svm = ##### create SVC with your kernel
svm.fit(d_train,c_train)
plt.figure()
print "score train = ", svm.score(d_train, c_train)
print "score test  = ", svm.score(d_test, c_test)
plot_boundary_with_data(svm,d_train,c_train, density=100j)

# ----------------------
# your code here
# plot the function f(x)=g*exp(-g*x**2)
# with values of g=0.1, 0.5, 1.0 and 2.0
# for the interval [-5,5]
# you may use the x array created just below
# ---------------------
x=np.arange(-5,5,0.1)

show()

