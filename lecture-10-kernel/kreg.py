import numpy as np
import matplotlib.pyplot as plt
   
def create_random_dataset (n_points, n_dims=1, xmin=0, xmax=1, exp=1, noise=0.01 ):
   theta = np.random.random(n_dims+1)/2-1
   X = np.hstack((np.ones((n_points,1)), np.random.random((n_points,n_dims))*(xmax-xmin)+xmin))
   y = np.sin(X.dot(theta)**exp) + np.random.normal(0,(xmax-xmin)*noise,n_points)
   return X,y,theta

def plot_regressor_prediction_in_1D(X, regressor, color="r", linewidth=3):
   i = np.argsort(X[:,1])
   plt.plot (X[i][:,1], regressor.predict(X[i]), color=color, linewidth=linewidth)

# closed form ridge regression
class K_RidgeRegression:
	def __init__ (self, lmbd=0.1, kernel=None):
	   self.lmbd   = lmbd	
	   self.kernel = kernel
   
	def dot(self,X1,X2):
	   return X1.dot(X2.T)

	def fit(self,X,y):
	   if self.kernel==None:
		   self.kernel=self.dot
	   self.Xtrain = X
	   I = np.eye(X.shape[0])
	   self.KIinv  = np.linalg.pinv(self.kernel(X,X)+self.lmbd*I)
	   self.ytrain = np.array([y]).T
	
	def predict(self,X):
	   return self.ytrain.T.dot(self.KIinv).dot(self.kernel(self.Xtrain,X))[0]

