from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np

# ------------------
# example function definition with partial derivatives
# ------------------
def f1(x,y):
  return 1-(x**2+y**2)/5

def df1x(x,y):
  return -x*2/5

def df1y(x,y):
  return -y*2/5

# ---------------------
# plot gradient descent iterations for two variables functions
# args:
#    f: name of two variables functions. must be defined as "def fname(x,y):"
#    dfx: name of the partial derivative of function f with respect to the first variable
#    dfx: name of the partial derivative of function f with respect to the second variable
#    xrange, yrange: x/y ranges for the plot
#    l: step size for the gradient descent
#    max_iters: maximum number of iterations
# ---------------------
def plotgd(f,dfx,dfy, init, xrange=[-2,2], yrange=[-2,2], l=0.01, max_iters=100):
  # -------------------
  # plot function
  # -------------------
  fig = plt.figure()
  ax = fig.gca(projection='3d')
  mpoints=40.0
  X, Y = np.meshgrid(np.arange(xrange[0],xrange[1], (xrange[1]-xrange[0])/mpoints), np.arange(yrange[0],yrange[1], (yrange[1]-yrange[0])/mpoints))
  Z = f(X,Y)
  ax.plot_wireframe(X,Y,Z, color="0.4")
  ax.set_zlim(np.min(Z),np.max(Z))
 
  # -------------------
  # plot gradient descent path
  # -------------------
  (last_step_size, iteration_number) = (1,1)
  [x,y] = np.array(init)*1.0
  ax.scatter(x,y,f(x,y))
  while ( (last_step_size>0.1) & (iteration_number<max_iters)):
     # compute x and y differentials in the direction of steepest ascent
     (dx,dy) = (dfx(x,y), dfy(x,y))

     # ----------- PART 1: MODIFY BELOW THIS LINE ------------
     # compute new x coordinate taking a step in the direction
     # of steepest ascent weighted by l
     x = 
     # likewise for y
     y = 
     # ----------- DO NOT MODIFY BELOW THIS LINE ------
     
     ax.scatter(x,y,f(x,y)) 
     last_step_size = np.sqrt(dx**2+dy**2)
     print "i="+str(iteration_number)+" d="+str(last_step_size)+" (x,y,z)="+str(x)+","+str(y)+","+str(f(x,y))
     iteration_number += 1

# plot gradient descent path for functin above, starting at point (1.5, 2)
plotgd(f1,df1x,df1y, init=(1.5,2), l=0.1)

# ------- PART 2: MODIFY BELOW THIS LINE ----------

# define function and partial derivaties similarly to f1, df1x, df1y
# call plotgd function according to assignment instructions

# ------- DO NOT MODIFY BELOW THIS LINE ----

plt.show()
