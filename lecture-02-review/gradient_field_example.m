1;

function r=f(x,y)
   r=1-(x.**2)-(y.**2);
endfunction

function r=dx(x)
   r=-2.*x;
endfunction

function r=dy(y)
   r=-2.*y;
endfunction


[x,y] = meshgrid (-1:.1:1);
figure(1);
mesh(x,y,f(x,y));
figure(2);
quiver (x,y,dx(x), dy(y));


